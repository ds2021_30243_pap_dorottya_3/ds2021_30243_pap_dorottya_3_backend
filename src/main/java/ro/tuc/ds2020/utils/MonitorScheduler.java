package ro.tuc.ds2020.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.MonitorValueDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.dtos.builders.MonitorValueBuilder;
import ro.tuc.ds2020.dtos.builders.SensorBuilder;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.MonitorValue;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.AccountRepository;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.SensorRepository;
import ro.tuc.ds2020.services.AccountService;
import ro.tuc.ds2020.services.DeviceService;
import ro.tuc.ds2020.services.MonitorValueService;
import ro.tuc.ds2020.services.SensorService;

import java.sql.Timestamp;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Component
public class MonitorScheduler implements ApplicationListener<ContextRefreshedEvent> {
    private final MonitorValueService monitorValueService;
    private final AccountRepository accountRepository;
    private final SensorRepository sensorRepository;
    private final DeviceRepository deviceRepository;
    private final DeviceService deviceService;
    private final AccountService accountService;
    private final SensorService sensorService;

    @Autowired
    public MonitorScheduler(MonitorValueService monitorValueService, SensorRepository sensorRepository, AccountRepository accountRepository, DeviceRepository deviceRepository, DeviceService deviceService, AccountService accountService, SensorService sensorService) {
        this.monitorValueService = monitorValueService;
        this.sensorRepository = sensorRepository;
        this.accountRepository = accountRepository;
        this.deviceRepository = deviceRepository;
        this.deviceService = deviceService;
        this.accountService = accountService;
        this.sensorService = sensorService;
    }

    @Override
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        // add default accounts
        Account admin = new Account("a", "a", 19, "a", "a", "ADMIN", null);
        Account client = new Account("c", "c", 19, "c", "c", "CLIENT", null);
        Account client1 = new Account("ccc", "cc", 120, "cc", "cc", "CLIENT", null);
        this.accountRepository.save(admin);
        this.accountRepository.save(client);
        this.accountRepository.save(client1);

        // add account device
        Device device = new Device("description", "device 1", 123, 21, client, null);
        DeviceDTO deviceBuilder = DeviceBuilder.toDeviceDTO(device);
        UUID deviceId = this.deviceService.insertClientDevice(deviceBuilder);

        Device device2 = new Device("description", "device 2", 13, 2111, client, null);
        DeviceDTO deviceBuilder2 = DeviceBuilder.toDeviceDTO(device2);
        UUID deviceId2 = this.deviceService.insertClientDevice(deviceBuilder2);

//        System.out.println(device.getId());
        // add sensors
        Sensor sensor = new Sensor("Test sensor", 130 /*, this.deviceRepository.findById(deviceId).get()*/);
        SensorDTO sensorBuilder = SensorBuilder.toSensorDTO(sensor);
        this.sensorService.insertDevicesSensor(sensorBuilder, deviceId);

        Sensor sensor2 = new Sensor("Test", 5 /*, this.deviceRepository.findById(deviceId).get()*/);
        SensorDTO sensorBuilder2 = SensorBuilder.toSensorDTO(sensor2);
        this.sensorService.insertDevicesSensor(sensorBuilder2, deviceId2);
//        UUID sensorID = this.sensorService.insertDevicesSensor(sensorBuilder);
        // sensor.setId(sensorID);
//        System.out.println(sensor.getDevice());

//        sensorRepository.deleteById(sensorID);

        //Put for the last few days values
        for(Sensor s: sensorRepository.findAll()) {
            for(int i = 0; i < 15; i++) {
                Random r = new Random();
                int low = 1;
                int high = (int) s.getMaximumValue() + 100;

                long offset = Timestamp.valueOf("2022-01-05 00:00:00").getTime();
                long end = Timestamp.valueOf("2022-01-14 00:00:00").getTime();
                long diff = end - offset + 1;
                Timestamp rand = new Timestamp(offset + (long)(Math.random() * diff));

                MonitorValue mv = new MonitorValue(
                        rand,
                        r.nextInt(high - low) + low,
                        s
                );
                MonitorValueDTO monitorValueDTO = MonitorValueBuilder.toMonitorValueDTO(mv);

                System.out.println(monitorValueDTO);

                monitorValueService.insertSensorsMonitorValue(monitorValueDTO);
            }
        }

        // add monitor values
        this.monitorValueService.generateValues();
    }
}
