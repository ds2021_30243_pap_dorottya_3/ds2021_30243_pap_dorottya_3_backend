package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Account;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AccountRepository extends JpaRepository<Account, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Account> findByName(String name);

    /**
     * Example: Write Custom Query
     */
    @Query(value = "SELECT p " +
            "FROM Account p " +
            "WHERE p.name = :name " +
            "AND p.age >= 60  ")
    Optional<Account> findSeniorsByName(@Param("name") String name);

    Optional<Account> findByAccountName(@Param("accountName") String accountName);
}
