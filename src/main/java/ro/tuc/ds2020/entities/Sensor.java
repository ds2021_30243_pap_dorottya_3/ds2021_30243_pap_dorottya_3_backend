package ro.tuc.ds2020.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@Entity
public class Sensor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "maximumValue")
    private float maximumValue;

    // @JsonBackReference
    // @JoinColumn(name="device")
    // @OneToOne //(fetch = FetchType.EAGER)
    // private Device device;

/*    @JoinColumn(name="device")
    @OneToOne
    private Device device;*/

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "sensor", cascade = CascadeType.REMOVE)
    @JsonManagedReference
    private Set<MonitorValue> monitorValues;

    public Sensor() {
    }

    public Sensor(String description, float maximumValue /*,Device device*/) {
        this.description = description;
        this.maximumValue = maximumValue;
        // this.device = device;
    }

    public void addMonitorValue(MonitorValue monitorValue) {
        this.monitorValues.add(monitorValue);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(float maximumValue) {
        this.maximumValue = maximumValue;
    }

    /*public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }*/

    public Set<MonitorValue> getMonitorValues() {
        return monitorValues;
    }

    public void setMonitorValues(Set<MonitorValue> monitorValues) {
        this.monitorValues = monitorValues;
    }
}
