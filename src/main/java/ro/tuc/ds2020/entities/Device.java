package ro.tuc.ds2020.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class Device implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "location", nullable = false)
    private String location;

    @Column(name = "maximumEnergyConsumption")
    private float maximumEnergyConsumption;

    @Column(name = "averageEnergyConsumption")
    private float averageEnergyConsumption;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    @JoinColumn(name="account_id")
    private Account account;

    // @JsonManagedReference
    @JoinColumn(name="sensorValue")
    @OneToOne /*( cascade = CascadeType.ALL, mappedBy = "device" ) */
    private Sensor sensorValue;

    public Device() {
    }

    public Device(String description, String location, float maximumEnergyConsumption, float averageEnergyConsumption, Account account, Sensor sensorValue) {
        this.description = description;
        this.location = location;
        this.maximumEnergyConsumption = maximumEnergyConsumption;
        this.averageEnergyConsumption = averageEnergyConsumption;
        this.account = account;
        this.sensorValue = sensorValue;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public float getMaximumEnergyConsumption() {
        return maximumEnergyConsumption;
    }

    public void setMaximumEnergyConsumption(float maximumEnergyConsumption) {
        this.maximumEnergyConsumption = maximumEnergyConsumption;
    }

    public float getAverageEnergyConsumption() {
        return averageEnergyConsumption;
    }

    public void setAverageEnergyConsumption(float averageEnergyConsumption) {
        this.averageEnergyConsumption = averageEnergyConsumption;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Sensor getSensor() {
        return sensorValue;
    }

    public void setSensor(Sensor sensorValue) {
        this.sensorValue = sensorValue;
    }
}
