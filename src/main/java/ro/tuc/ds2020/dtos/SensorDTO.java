package ro.tuc.ds2020.dtos;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.MonitorValue;

import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class SensorDTO extends RepresentationModel<SensorDTO> {

    private UUID id;
    private String description;
    private float maximumValue;
   //private Device device;
    private Set<MonitorValue> monitorValues;

    public SensorDTO() {
    }

    public SensorDTO(UUID id, String description, float maximumValue, /*Device device,*/ Set<MonitorValue> monitorValues) {
        this.id = id;
        this.description = description;
        this.maximumValue = maximumValue;
        //this.device = device;
        this.monitorValues = monitorValues;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(float maximumValue) {
        this.maximumValue = maximumValue;
    }

/*    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }*/

    public Set<MonitorValue> getMonitorValues() {
        return monitorValues;
    }

    public void setMonitorValues(Set<MonitorValue> monitorValues) {
        this.monitorValues = monitorValues;
    }

    @Override
    public String toString() {
        return "SensorDTO{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", maximumValue=" + maximumValue +
                //", device=" + device +
                ", monitorValues=" + monitorValues +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SensorDTO sensorDTO = (SensorDTO) o;
        return Float.compare(sensorDTO.maximumValue, maximumValue) == 0 &&
                Objects.equals(description, sensorDTO.description) &&
                // Objects.equals(device, sensorDTO.device) &&
                Objects.equals(monitorValues, sensorDTO.monitorValues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, maximumValue,/* device,*/ monitorValues);
    }
}
