package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MonitorValueDTO;
import ro.tuc.ds2020.entities.MonitorValue;

public class MonitorValueBuilder {
    public MonitorValueBuilder() {
    }

    public static MonitorValueDTO toMonitorValueDTO(MonitorValue monitorValue) {
        return new MonitorValueDTO(monitorValue.getId(), monitorValue.getTimeStamp(), monitorValue.getValue(), monitorValue.getSensor());
    }

    public static MonitorValue toEntity(MonitorValueDTO monitorValueDTO) {
        return new MonitorValue(monitorValueDTO.getTimeStamp(),
                monitorValueDTO.getValue(),
                monitorValueDTO.getSensor());
    }
}
