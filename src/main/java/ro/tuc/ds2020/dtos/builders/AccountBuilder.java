package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.AccountDetailsDTO;
import ro.tuc.ds2020.dtos.AccountLoginDTO;
import ro.tuc.ds2020.entities.Account;

public class AccountBuilder {

    private AccountBuilder() {
    }

    public static AccountDTO toAccountDTO(Account account) {
        return new AccountDTO(account.getId(), account.getName(), account.getAge(), account.getDevices());
    }

    public static AccountLoginDTO toAccountLoginDTO(Account account) {
        return new AccountLoginDTO(account.getAccountName(), account.getPassword());
    }

    public static AccountDetailsDTO toAccountDetailsDTO(Account account) {
        return new AccountDetailsDTO(account.getId(), account.getName(), account.getAddress(), account.getAge(), account.getAccountName(), account.getPassword(), account.getType(), account.getDevices());
    }

    public static Account toEntity(AccountDetailsDTO accountDetailsDTO) {
        return new Account(accountDetailsDTO.getName(),
                accountDetailsDTO.getAddress(),
                accountDetailsDTO.getAge(),
                accountDetailsDTO.getAccountName(),
                accountDetailsDTO.getPassword(),
                accountDetailsDTO.getType(),
                accountDetailsDTO.getDevices());
    }
}
