package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Sensor;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.Objects;
import java.util.UUID;

public class DeviceDTO extends RepresentationModel<DeviceDTO> {
    private UUID id;
    private String description;
    private String location;
    private float maximumEnergyConsumption;
    private float averageEnergyConsumption;
    private Account account;
    private Sensor sensor;

    public DeviceDTO() {
    }

    public DeviceDTO(UUID id, String description, String location, float maximumEnergyConsumption, float averageEnergyConsumption, Account account, Sensor sensor) {
        this.id = id;
        this.description = description;
        this.location = location;
        this.maximumEnergyConsumption = maximumEnergyConsumption;
        this.averageEnergyConsumption = averageEnergyConsumption;
        this.account = account;
        this.sensor = sensor;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public float getMaximumEnergyConsumption() {
        return maximumEnergyConsumption;
    }

    public void setMaximumEnergyConsumption(float maximumEnergyConsumption) {
        this.maximumEnergyConsumption = maximumEnergyConsumption;
    }

    public float getAverageEnergyConsumption() {
        return averageEnergyConsumption;
    }

    public void setAverageEnergyConsumption(float averageEnergyConsumption) {
        this.averageEnergyConsumption = averageEnergyConsumption;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }

    @Override
    public String toString() {
        return "DeviceDTO{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", location='" + location + '\'' +
                ", maximumEnergyConsumption=" + maximumEnergyConsumption +
                ", averageEnergyConsumption=" + averageEnergyConsumption +
                ", account=" + account +
                ", sensor=" + sensor +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DeviceDTO deviceDTO = (DeviceDTO) o;
        return Float.compare(deviceDTO.maximumEnergyConsumption, maximumEnergyConsumption) == 0 &&
                Float.compare(deviceDTO.averageEnergyConsumption, averageEnergyConsumption) == 0 &&
                Objects.equals(description, deviceDTO.description) &&
                Objects.equals(location, deviceDTO.location) &&
                Objects.equals(account, deviceDTO.account) &&
                Objects.equals(sensor, deviceDTO.sensor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), description, location, maximumEnergyConsumption, averageEnergyConsumption, account, sensor);
    }
}
