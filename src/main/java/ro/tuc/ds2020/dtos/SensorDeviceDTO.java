package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Sensor;

import java.util.UUID;

public class SensorDeviceDTO extends RepresentationModel<SensorDeviceDTO> {

    public SensorDTO sensor;
    public UUID deviceId;

}
