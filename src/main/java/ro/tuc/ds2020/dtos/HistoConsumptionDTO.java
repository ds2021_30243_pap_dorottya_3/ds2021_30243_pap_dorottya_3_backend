package ro.tuc.ds2020.dtos;

import java.sql.Timestamp;
import java.util.UUID;

public class HistoConsumptionDTO {
    private UUID sensorId;
    private Timestamp timeStamp;
    private float value;

    public HistoConsumptionDTO(UUID sensorId, Timestamp timeStamp, float value) {
        this.sensorId = sensorId;
        this.timeStamp = timeStamp;
        this.value = value;
    }

    public UUID getSensorId() {
        return sensorId;
    }

    public void setSensorId(UUID sensorId) {
        this.sensorId = sensorId;
    }

    public Timestamp getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
