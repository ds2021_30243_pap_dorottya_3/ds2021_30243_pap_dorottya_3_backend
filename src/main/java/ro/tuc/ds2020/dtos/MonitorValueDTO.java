package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Sensor;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

public class MonitorValueDTO extends RepresentationModel<MonitorValueDTO> {
    private UUID id;
    private Timestamp timeStamp;
    private float value;
    private Sensor sensor;

    public MonitorValueDTO() {
    }

    public MonitorValueDTO(UUID id, Timestamp timeStamp, float value, Sensor sensor) {
        this.id = id;
        this.timeStamp = timeStamp;
        this.value = value;
        this.sensor = sensor;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Timestamp getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }

    @Override
    public String toString() {
        return "MonitorValueDTO{" +
                "id=" + id +
                ", timeStamp=" + timeStamp +
                ", value=" + value +
                ", sensor=" + sensor +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MonitorValueDTO that = (MonitorValueDTO) o;
        return Float.compare(that.value, value) == 0 &&
                Objects.equals(timeStamp, that.timeStamp) &&
                Objects.equals(sensor, that.sensor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timeStamp, value, sensor);
    }
}
