package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.dtos.validators.annotation.AgeLimit;
import ro.tuc.ds2020.entities.Device;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class AccountDetailsDTO extends RepresentationModel<AccountDetailsDTO> {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private String address;
    @AgeLimit(limit = 18)
    private int age;
    @NotNull
    private String accountName;
    @NotNull
    private String password;
    @NotNull
    private String type;
    Set<Device> devices;

    public AccountDetailsDTO() {
    }

    public AccountDetailsDTO(String name, String address, int age, String accountName, String password, String type, Set<Device> devices) {
        this.name = name;
        this.address = address;
        this.age = age;
        this.accountName = accountName;
        this.password = password;
        this.type = type;
        this.devices = devices;
    }

    public AccountDetailsDTO(UUID id, String name, String address, int age, String accountName, String password, String type, Set<Device> devices) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.age = age;
        this.accountName = accountName;
        this.password = password;
        this.type = type;
        this.devices = devices;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<Device> getDevices() {
        return devices;
    }

    public void setDevices(Set<Device> devices) {
        this.devices = devices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountDetailsDTO that = (AccountDetailsDTO) o;
        return age == that.age && Objects.equals(name, that.name) &&
                Objects.equals(address, that.address) &&
                Objects.equals(accountName, that.accountName) &&
                Objects.equals(password, that.password) &&
                Objects.equals(type, that.type) &&
                Objects.equals(devices, that.devices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, age, accountName, password, type, devices);
    }

    @Override
    public String toString() {
        return "AccountDetailsDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", age=" + age +
                ", accountName='" + accountName + '\'' +
                ", password='" + password + '\'' +
                ", type='" + type + '\'' +
                ", devices=" + devices +
                '}';
    }

}
