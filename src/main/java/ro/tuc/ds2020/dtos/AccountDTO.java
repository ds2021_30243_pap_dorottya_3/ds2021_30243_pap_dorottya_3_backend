package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Device;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class AccountDTO extends RepresentationModel<AccountDTO> {
    private UUID id;
    private String name;
    private int age;
    Set<Device> devices;

    public AccountDTO() {
    }

    public AccountDTO(UUID id, String name, int age, Set<Device> devices) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.devices = devices;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Set<Device> getDevices() {
        return devices;
    }

    public void setDevices(Set<Device> devices) {
        this.devices = devices;
    }

    @Override
    public String toString() {
        return "AccountDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", devices=" + devices +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AccountDTO that = (AccountDTO) o;
        return age == that.age && Objects.equals(name, that.name) &&
                Objects.equals(devices, that.devices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, age, devices);
    }
}
