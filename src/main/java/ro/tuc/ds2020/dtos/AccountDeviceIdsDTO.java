package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.util.UUID;

public class AccountDeviceIdsDTO extends RepresentationModel<AccountDeviceIdsDTO> {

    public UUID accountId;
    public UUID deviceId;

}
