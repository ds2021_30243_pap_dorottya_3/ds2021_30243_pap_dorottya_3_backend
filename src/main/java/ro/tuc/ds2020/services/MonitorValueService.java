package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import ro.tuc.ds2020.constants.NotificationEndpoints;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MonitorValueDTO;
import ro.tuc.ds2020.dtos.builders.MonitorValueBuilder;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.MonitorValue;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.AccountRepository;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.MonitorValueRepository;
import ro.tuc.ds2020.repositories.SensorRepository;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MonitorValueService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MonitorValueService.class);
    private final MonitorValueRepository monitorValueRepository;
    private final SensorRepository sensorRepository;
    private final AccountRepository accountRepository;
    private final DeviceRepository deviceRepository;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    public MonitorValueService(MonitorValueRepository monitorValueRepository, SensorRepository sensorRepository, AccountRepository accountRepository, DeviceRepository deviceRepository) {
        this.monitorValueRepository = monitorValueRepository;
        this.sensorRepository = sensorRepository;
        this.accountRepository = accountRepository;
        this.deviceRepository = deviceRepository;
    }

    public List<MonitorValueDTO> findMonitorValues() {
        List<MonitorValue> monitorValueList = monitorValueRepository.findAll();
        return monitorValueList.stream()
                .map(MonitorValueBuilder::toMonitorValueDTO)
                .collect(Collectors.toList());
    }

    public MonitorValueDTO findMonitorValueById(UUID id) {
        Optional<MonitorValue> monitorValueOptional = monitorValueRepository.findById(id);
        if (!monitorValueOptional.isPresent()) {
            LOGGER.error("MonitorValue with id {} was not found in db", id);
            throw new ResourceNotFoundException(MonitorValue.class.getSimpleName() + " with id: " + id);
        }
        return MonitorValueBuilder.toMonitorValueDTO(monitorValueOptional.get());
    }

    public UUID insert(MonitorValueDTO monitorValueDTO) {
        MonitorValue monitorValue = MonitorValueBuilder.toEntity(monitorValueDTO);
        monitorValue = monitorValueRepository.save(monitorValue);
        LOGGER.debug("MonitorValue with id {} was inserted in db", monitorValue.getId());
        return monitorValue.getId();
    }

    public List<MonitorValueDTO> insertCurrentMonitorValueForAccount(UUID accountId) {
        Account account = accountRepository.findById(accountId).get();
        List<MonitorValueDTO> monitorValues = new ArrayList<MonitorValueDTO>();
        System.out.println(account.getId());

        for (Device device : account.getDevices()) {
            Sensor sensor = device.getSensor();
            if(!ObjectUtils.isEmpty(sensor)) {
                Random r = new Random();
                int low = 1;
                int high = (int) sensor.getMaximumValue() + 100;

                MonitorValue mv = new MonitorValue(
                        new Timestamp(System.currentTimeMillis()),
                        r.nextInt(high - low) + low,
                        sensor
                );
                MonitorValueDTO monitorValueDTO = MonitorValueBuilder.toMonitorValueDTO(mv);

                insertSensorsMonitorValue(monitorValueDTO);
                monitorValues.add(monitorValueDTO);

                // device part
                float totalConsumption = 0;
                int totalMonitorValue = 0;
                for (MonitorValue monitorValue : sensor.getMonitorValues()) {
                    totalConsumption += monitorValue.getValue();
                    totalMonitorValue ++;
                }

                device.setMaximumEnergyConsumption(totalConsumption);
                device.setAverageEnergyConsumption(totalConsumption / totalMonitorValue);
                deviceRepository.save(device);

                // web socket
                if(mv.getValue() >= (int) sensor.getMaximumValue()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Sensor " + mv.getSensor().getId() + " monitor value limit exceeded(with value: " + mv.getValue() + ")");

                    this.template.convertAndSend(NotificationEndpoints.MONITOR_VALUE_ADDITION + device.getAccount().getId(), sb);
                }

            }
        }
        return monitorValues;
    }

    public UUID insertSensorsMonitorValue(MonitorValueDTO monitorValueDTO) {
        Sensor sensor = sensorRepository.findById(monitorValueDTO.getSensor().getId()).get();
        MonitorValue monitorValue = MonitorValueBuilder.toEntity(monitorValueDTO);
        monitorValue = monitorValueRepository.save(monitorValue);

        sensor.setMonitorValues(Collections.singleton(monitorValue));
        sensorRepository.save(sensor);

        LOGGER.debug("MonitorValue with id {} was inserted in db", monitorValue.getId());
        return monitorValue.getId();
    }

    public UUID update(MonitorValueDTO monitorValueDTO) {
        UUID monitorValueId = monitorValueDTO.getId();

        Optional<MonitorValue> monitorValue = monitorValueRepository.findById(monitorValueId);
        if (!monitorValue.isPresent()) {
            LOGGER.error("MonitorValue with id {} was not found in db", monitorValueId);
            throw new ResourceNotFoundException(MonitorValue.class.getSimpleName() + " with id: " + monitorValueId);
        }

        monitorValue.get().setTimeStamp(monitorValueDTO.getTimeStamp());
        monitorValue.get().setValue(monitorValueDTO.getValue());

        monitorValueRepository.save(monitorValue.get());

        LOGGER.debug("MonitorValue with id {} was updated in db", monitorValueId);
        return monitorValueId;
    }

    public void delete(UUID monitorValueId) {
        Optional<MonitorValue> monitorValue = monitorValueRepository.findById(monitorValueId);
        if (!monitorValue.isPresent()) {
            LOGGER.error("MonitorValue with id {} was not found in db", monitorValueId);
            throw new ResourceNotFoundException(MonitorValue.class.getSimpleName() + " with id: " + monitorValueId);
        }

        monitorValueRepository.deleteById(monitorValue.get().getId());
    }

    public void generateValues() {
        Thread t1 = new Thread(this.createRunnable(this.sensorRepository, this.monitorValueRepository, this.template));
        t1.start();
    }

    private Runnable createRunnable(final SensorRepository sensorRepository, final MonitorValueRepository monitorValueRepository, final SimpMessagingTemplate template) {
        Runnable aRunnable = new Runnable() {
            public void run() {
                while (true) {

                    sensorRepository.findAll().forEach(sensor -> {
                        Random r = new Random();
                        int low = 1;
                        int high = (int) sensor.getMaximumValue() + 100;

                        long offset = Timestamp.valueOf("2015-01-01 00:00:00").getTime();
                        long end = Timestamp.valueOf("2022-01-14 00:00:00").getTime();
                        long diff = end - offset + 1;
                        Timestamp rand = new Timestamp(offset + (long)(Math.random() * diff));

                        MonitorValue mv = new MonitorValue(
                                rand,
                                r.nextInt(high - low) + low,
                                sensor
                        );
                        MonitorValueDTO monitorValueDTO = MonitorValueBuilder.toMonitorValueDTO(mv);

                        insertSensorsMonitorValue(monitorValueDTO);

                        // web socket
                        if(mv.getValue() >= (int) sensor.getMaximumValue()) {
                            List<Device> devices = deviceRepository.findAll();

                            for (Device device : devices) {
                                if (!ObjectUtils.isEmpty(device.getSensor())) {
                                    if (device.getSensor().getId() == sensor.getId()) {
                                        StringBuilder sb = new StringBuilder();
                                        sb.append("Sensor " + mv.getSensor().getId() + " monitor value limit exceeded(with value: " + mv.getValue() + ")");

                                        template.convertAndSend(NotificationEndpoints.MONITOR_VALUE_ADDITION + device.getAccount().getId(), sb);
                                    }
                                }
                            }
                        }

                        // device part
                        float totalConsumption = 0;
                        int totalMonitorValue = 0;
                        for (MonitorValue monitorValue : sensor.getMonitorValues()) {
                            totalConsumption += monitorValue.getValue();
                            totalMonitorValue ++;
                        }

                        List<Device> devices = deviceRepository.findAll();
                        for (Device device : devices) {
                            if(!ObjectUtils.isEmpty(device.getSensor())) {
                                if(device.getSensor().getId() == sensor.getId()) {
                                    device.setMaximumEnergyConsumption(totalConsumption);
                                    device.setAverageEnergyConsumption(totalConsumption / totalMonitorValue);
                                    deviceRepository.save(device);
                                }
                            }
                        }

                    });

                    try {
                        Thread.sleep(100000 * 60);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        return aRunnable;
    }
}
