package ro.tuc.ds2020.services.impl;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.HistoConsumptionDTO;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.MonitorValue;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.AccountRepository;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.MonitorValueRepository;
import ro.tuc.ds2020.repositories.SensorRepository;
import ro.tuc.ds2020.services.DeviceService;
import ro.tuc.ds2020.services.JsonRPCService;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AutoJsonRpcServiceImpl
public class JsonRPCServiceImpl implements JsonRPCService {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private DeviceRepository deviceRepository;
    @Autowired
    private SensorRepository sensorRepository;
    @Autowired
    private MonitorValueRepository monitorValueRepository;
    @Autowired
    private DeviceService deviceService;

    public JsonRPCServiceImpl(AccountRepository accountRepository, DeviceRepository deviceRepository, SensorRepository sensorRepository, MonitorValueRepository monitorValueRepository, DeviceService deviceService) {
        this.accountRepository = accountRepository;
        this.deviceRepository = deviceRepository;
        this.sensorRepository = sensorRepository;
        this.monitorValueRepository = monitorValueRepository;
        this.deviceService = deviceService;
    }

    @Override
    public int multiplier(int a, int b) {
        return a * b;
    }

    @Override
    public List<HistoConsumptionDTO> getHistoricalConsumption(UUID id, int days) {
        /*System.out.println(id);
        List<Account> accounts = accountRepository.findAll();
        for(Account account: accounts) {
            System.out.println(account.getId());
        }*/

        Optional<Account> account = accountRepository.findById(id);
        List<HistoConsumptionDTO> histoConsumptionDTOList = new ArrayList<>();

        for(Device device: account.get().getDevices()) {
            Sensor sensor = device.getSensor();

            if(!ObjectUtils.isEmpty(sensor)) {
                Timestamp currentDate = new Timestamp(System.currentTimeMillis());
                Timestamp lastHistoryTimeStamp = Timestamp.valueOf(currentDate.toLocalDateTime().minusDays(days - 1));
                System.out.println(lastHistoryTimeStamp);

                System.out.println(currentDate);
                for(MonitorValue monitorValue: sensor.getMonitorValues()) {
                    if(lastHistoryTimeStamp.toLocalDateTime().isBefore(monitorValue.getTimeStamp().toLocalDateTime())) {
                        histoConsumptionDTOList.add(new HistoConsumptionDTO(sensor.getId(), monitorValue.getTimeStamp(), monitorValue.getValue()));
                    }
                }
            }
        }

        for(HistoConsumptionDTO histoConsumptionDTO: histoConsumptionDTOList) {
            System.out.println(histoConsumptionDTO.getSensorId());
        }

        return histoConsumptionDTOList;
    }

    @Override
    public float getAverageConsumptionForLast7Days(UUID id) {
        System.out.println(id);
        Optional<Account> account = accountRepository.findById(id);

        Float sumLast7Days = 0.0f;
        Timestamp currentDate = new Timestamp(System.currentTimeMillis());
        Timestamp sevenDaysAgoDate = Timestamp.valueOf(currentDate.toLocalDateTime().minusDays(6));

        for(Device device: account.get().getDevices()) {
            Sensor sensor = device.getSensor();

            for(MonitorValue mv: sensor.getMonitorValues()) {
                if(sevenDaysAgoDate.toLocalDateTime().isBefore(mv.getTimeStamp().toLocalDateTime())
                        && currentDate.toLocalDateTime().isAfter(mv.getTimeStamp().toLocalDateTime())) {
                    sumLast7Days += mv.getValue();
                }
            }
        }

        System.out.println(sumLast7Days);
        return (sumLast7Days / 7.0f);
    }
}
