package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.builders.SensorBuilder;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.SensorRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SensorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SensorService.class);
    private final SensorRepository sensorRepository;
    private final DeviceRepository deviceRepository;

    @Autowired
    public SensorService(SensorRepository sensorRepository, DeviceRepository deviceRepository) {
        this.sensorRepository = sensorRepository;
        this.deviceRepository = deviceRepository;
    }

    public List<SensorDTO> findSensors() {
        List<Sensor> sensorList = sensorRepository.findAll();
        return sensorList.stream()
                .map(SensorBuilder::toSensorDTO)
                .collect(Collectors.toList());
    }

    public List<SensorDTO> findAvailableSensors() {
        List<Sensor> sensorList = sensorRepository.findAll();
        List<Sensor> availableSensors = new ArrayList<Sensor>();

        List<Sensor> nonAvaiableSensors = new ArrayList<Sensor>();
        for(Device device: deviceRepository.findAll()) {
            if(device.getSensor() != null) {
                nonAvaiableSensors.add(device.getSensor());
            }
        }
//        for (Sensor n: nonAvaiableSensors) {
//            System.out.println(n);
//        }

        int ok = 1;
        for(Sensor sensor: sensorList) {
            ok = 1;
            for(Sensor nonAvailableSensor : nonAvaiableSensors) {
                if(sensor.getId().equals(nonAvailableSensor.getId())) {
                    ok = 0;
                }
            }

            if(ok == 1) {
                availableSensors.add(sensor);
            }
        }

        return availableSensors.stream()
                .map(SensorBuilder::toSensorDTO)
                .collect(Collectors.toList());
    }

    public SensorDTO findSensorById(UUID id) {
        Optional<Sensor> prosumerOptional = sensorRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Sensor with id {} was not found in db", id);
            throw new ResourceNotFoundException(Sensor.class.getSimpleName() + " with id: " + id);
        }
        return SensorBuilder.toSensorDTO(prosumerOptional.get());
    }

    public UUID insert(SensorDTO sensorDTO) {
        Sensor sensor = SensorBuilder.toEntity(sensorDTO);
        sensor = sensorRepository.save(sensor);
        LOGGER.debug("Sensor with id {} was inserted in db", sensor.getId());
        return sensor.getId();
    }

    public UUID insertDevicesSensor(SensorDTO sensorDTO, UUID deviceId) {
        Device device = deviceRepository.findById(deviceId).get();
        Sensor sensor = SensorBuilder.toEntity(sensorDTO);

        sensor = sensorRepository.save(sensor);
        device.setSensor(sensor);
        deviceRepository.save(device);

        //sensor.setDevice(device);

//        device = deviceRepository.save(device);
        // System.out.println(device.getSensor());
        // sensor.setId(UUID.randomUUID());
        //System.out.println("\n\n\n\n\n"+ sensor.getId() + " " +  sensor.getDescription()  + " " + sensor.getMaximumValue()  + " " + sensor.getDevice());

        return sensor.getId();
    }

    public UUID update(SensorDTO sensorDTO) {
        UUID sensorId = sensorDTO.getId();

        Optional<Sensor> sensor = sensorRepository.findById(sensorId);
        if (!sensor.isPresent()) {
            LOGGER.error("Sensor with id {} was not found in db", sensorId);
            throw new ResourceNotFoundException(Sensor.class.getSimpleName() + " with id: " + sensorId);
        }

        sensor.get().setDescription(sensorDTO.getDescription());
        sensor.get().setMaximumValue(sensorDTO.getMaximumValue());
        //sensor.get().setDevice(sensorDTO.getDevice());

        sensorRepository.save(sensor.get());

        LOGGER.debug("Sensor with id {} was updated in db", sensorId);
        return sensorId;
    }

    public void delete(UUID sensorId) {
        Optional<Sensor> sensor = sensorRepository.findById(sensorId);
        if (!sensor.isPresent()) {
            LOGGER.error("Sensor with id {} was not found in db", sensorId);
            throw new ResourceNotFoundException(Sensor.class.getSimpleName() + " with id: " + sensorId);
        }

        for (Device device : deviceRepository.findAll()) {
            if(device.getSensor() != null) {
                if(device.getSensor().getId().equals(sensorId)) {
                    device.setSensor(null);
                    deviceRepository.save(device);
                }
            }
        }

        sensorRepository.deleteById(sensor.get().getId());
    }
}
