package ro.tuc.ds2020.services;

import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;
import ro.tuc.ds2020.dtos.HistoConsumptionDTO;


import java.util.List;
import java.util.UUID;

@JsonRpcService("/externalDevice")
public interface JsonRPCService {

    int multiplier(@JsonRpcParam(value = "a") int a, @JsonRpcParam(value = "b") int b);
    List<HistoConsumptionDTO> getHistoricalConsumption(@JsonRpcParam(value = "id") UUID id, @JsonRpcParam(value = "days") int days);
    float getAverageConsumptionForLast7Days(@JsonRpcParam(value =  "id") UUID id);
}
