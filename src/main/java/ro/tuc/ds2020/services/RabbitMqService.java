package ro.tuc.ds2020.services;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.MonitorValueDTO;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.SensorRepository;

import java.sql.Timestamp;
import java.util.UUID;

@Service
public class RabbitMqService {
    @Autowired
    private MonitorValueService monitorValueService;

    @Autowired
    private SensorRepository sensorRepository;

    public RabbitMqService(MonitorValueService monitorValueService, SensorRepository sensorRepository) {
        this.monitorValueService = monitorValueService;
        this.sensorRepository = sensorRepository;
    }

    @Bean
    public void subscribeToMessages() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqps://itixjdms:X0ob4mYK9o1l4ttXw7i9SMPrhAjJlk8g@kangaroo.rmq.cloudamqp.com/itixjdms");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare("hello-world", false, false, false, null);
        System.out.println(" Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" Received '" + message + "'");

            // Split by values
            String[] tokens = message.split(",");

            // Parsing for date
            String[] timeStamp = tokens[0].split("=");
            System.out.println(timeStamp[1]);

            // Parsing for sensor id
            String[] sensorId = tokens[1].split("'");
            System.out.println(sensorId[1]);

            // Parsing for value
            String[] value = tokens[2].split("=");
            String[] finalValue = value[1].split("}");

            System.out.println(finalValue[0]);

            // Creating new monitor value
            if(sensorRepository.findById(UUID.fromString(sensorId[1])).get() != null) {
                Sensor sensor = sensorRepository.findById(UUID.fromString(sensorId[1])).get();
                MonitorValueDTO monitorValueDTO = new MonitorValueDTO();
                monitorValueDTO.setSensor(sensor);
                monitorValueDTO.setValue(Float.parseFloat(finalValue[0]));
                monitorValueDTO.setTimeStamp(Timestamp.valueOf(timeStamp[1]));
                monitorValueService.insertSensorsMonitorValue(monitorValueDTO);
            }
        };
        channel.basicConsume("hello-world", true, deliverCallback, consumerTag -> { });
    }
}
