package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.AccountDetailsDTO;
import ro.tuc.ds2020.dtos.AccountDeviceIdsDTO;
import ro.tuc.ds2020.dtos.builders.AccountBuilder;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.repositories.AccountRepository;
import ro.tuc.ds2020.repositories.DeviceRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AccountService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountService.class);
    private final AccountRepository accountRepository;
    private final DeviceRepository deviceRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository, DeviceRepository deviceRepository) {
        this.accountRepository = accountRepository;
        this.deviceRepository = deviceRepository;
    }

    public List<AccountDTO> findAccounts() {
        List<Account> accountList = accountRepository.findAll();
        return accountList.stream()
                .map(AccountBuilder::toAccountDTO)
                .collect(Collectors.toList());
    }

    public AccountDetailsDTO findAccountById(UUID id) {
        Optional<Account> prosumerOptional = accountRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Account with id {} was not found in db", id);
            throw new ResourceNotFoundException(Account.class.getSimpleName() + " with id: " + id);
        }
        return AccountBuilder.toAccountDetailsDTO(prosumerOptional.get());
    }

    public UUID insert(AccountDetailsDTO accountDTO) {
        Account account = AccountBuilder.toEntity(accountDTO);
        account = accountRepository.save(account);
        LOGGER.debug("Account with id {} was inserted in db", account.getId());
        return account.getId();
    }

    public UUID insertDeviceToClient(AccountDeviceIdsDTO accountDeviceIdsDTO) {
        UUID accountId = accountDeviceIdsDTO.accountId;
        UUID deviceId = accountDeviceIdsDTO.deviceId;
        System.out.println(accountId);
        System.out.println(deviceId);

        Account account = accountRepository.findById(accountId).get();
        Device device = deviceRepository.findById(deviceId).get();

        device.setAccount(account);
        deviceRepository.save(device);

        LOGGER.debug("Account with id {} was updated in db", accountId);
        return accountId;

    }

    public UUID update(AccountDetailsDTO accountDTO) {
        UUID accountId = accountDTO.getId();

        Optional<Account> account = accountRepository.findById(accountId);
        if (!account.isPresent()) {
            LOGGER.error("Account with id {} was not found in db", accountId);
            throw new ResourceNotFoundException(Account.class.getSimpleName() + " with id: " + accountId);
        }

        account.get().setName(accountDTO.getName());
        account.get().setAddress(accountDTO.getAddress());
        account.get().setAge(accountDTO.getAge());

        accountRepository.save(account.get());

        LOGGER.debug("Account with id {} was updated in db", accountId);
        return accountId;
    }

    public void delete(UUID accountId) {
        Optional<Account> account = accountRepository.findById(accountId);
        if (!account.isPresent()) {
            LOGGER.error("Account with id {} was not found in db", accountId);
            throw new ResourceNotFoundException(Account.class.getSimpleName() + " with id: " + accountId);
        }

        System.out.println(account.get().getId());
        accountRepository.deleteById(account.get().getId());
    }

    public AccountDetailsDTO findAccountByUsername(String accountName) {
        Optional<Account> prosumerOptional = accountRepository.findByAccountName(accountName);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Account with accountName {} was not found in db", accountName);
            throw new ResourceNotFoundException(Account.class.getSimpleName() + " with accountName: " + accountName);
        }
        return AccountBuilder.toAccountDetailsDTO(prosumerOptional.get());
    }

    public List<AccountDetailsDTO> findClientAccounts() {
        List<Account> accountList = accountRepository.findAll();
        return accountList.stream()
                .map(AccountBuilder::toAccountDetailsDTO)
                .collect(Collectors.toList());
    }
}
