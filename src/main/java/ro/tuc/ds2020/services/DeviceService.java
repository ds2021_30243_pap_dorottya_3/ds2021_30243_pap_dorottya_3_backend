package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.AccountDeviceIdsDTO;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceSensorIdsDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.AccountRepository;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.SensorRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DeviceService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceService.class);
    private final DeviceRepository deviceRepository;
    private final AccountRepository accountRepository;
    private final SensorRepository sensorRepository;

    @Autowired
    public DeviceService(DeviceRepository deviceRepository, AccountRepository accountRepository, SensorRepository sensorRepository) {
        this.deviceRepository = deviceRepository;
        this.accountRepository = accountRepository;
        this.sensorRepository = sensorRepository;
    }

    public List<DeviceDTO> findDevices() {
        List<Device> deviceList = deviceRepository.findAll();
        return deviceList.stream()
                .map(DeviceBuilder::toDeviceDTO)
                .collect(Collectors.toList());
    }

    public List<DeviceDTO> findAvailableDevices() {
        List<Device> deviceList = deviceRepository.findAll();
        List<Device> availableDevices = new ArrayList<Device>();
        for(Device device: deviceList) {
            if(device.getAccount() == null) {
                availableDevices.add(device);
            }
        }

        return availableDevices.stream()
                .map(DeviceBuilder::toDeviceDTO)
                .collect(Collectors.toList());
    }

    public List<DeviceDTO> getAvailableDevicesWithAccount() {
        List<Device> deviceList = deviceRepository.findAll();
        List<Device> availableDevices = new ArrayList<Device>();
        for(Device device: deviceList) {
            if(device.getAccount() != null && device.getSensor() == null) {
                availableDevices.add(device);
            }
        }

        return availableDevices.stream()
                .map(DeviceBuilder::toDeviceDTO)
                .collect(Collectors.toList());
    }

    public DeviceDTO findDeviceById(UUID id) {
        Optional<Device> deviceOptional = deviceRepository.findById(id);
        if (!deviceOptional.isPresent()) {
            LOGGER.error("Device with id {} was not found in db", id);
            throw new ResourceNotFoundException(Device.class.getSimpleName() + " with id: " + id);
        }
        return DeviceBuilder.toDeviceDTO(deviceOptional.get());
    }

    /*public SensorDTO findSensorByDeviceId(UUID deviceId) {
        Optional<Device> deviceOptional = deviceRepository.findById(deviceId);
        List<Sensor> sensors = sensorRepository.findAll();

        for (Device sensor: sensors) {
            // System.out.println(sensor.getDevice().getId());
            if(sensor.getDevice().getId().equals(deviceId)) {
                return SensorBuilder.toSensorDTO(sensor);
            }
        }
        return null;
    }*/

    public UUID insert(DeviceDTO deviceDTO) {
        Device device = DeviceBuilder.toEntity(deviceDTO);
        device = deviceRepository.save(device);

        LOGGER.debug("Device with id {} was inserted in db", device.getId());
        return device.getId();
    }

    public UUID insertSensorToDevice(DeviceSensorIdsDTO deviceSensorIdsDTO) {
        UUID deviceId = deviceSensorIdsDTO.deviceId;
        UUID sensorId = deviceSensorIdsDTO.sensorId;

        Device device = deviceRepository.findById(deviceId).get();
        Sensor sensor = sensorRepository.findById(sensorId).get();

        device.setSensor(sensor);
        deviceRepository.save(device);

        LOGGER.debug("Device with id {} was updated in db", deviceId);
        return deviceId;

    }

    public UUID insertClientDevice(DeviceDTO deviceDTO) {
        Account account = accountRepository.findById(deviceDTO.getAccount().getId()).get();
        Device device = DeviceBuilder.toEntity(deviceDTO);
        device = deviceRepository.save(device);

        account.setDevices(Collections.singleton(device));
        accountRepository.save(account);

        return device.getId();
    }

    public UUID update(DeviceDTO deviceDTO) {
        UUID deviceId = deviceDTO.getId();

        Optional<Device> device = deviceRepository.findById(deviceId);
        if (!device.isPresent()) {
            LOGGER.error("Device with id {} was not found in db", deviceId);
            throw new ResourceNotFoundException(Device.class.getSimpleName() + " with id: " + deviceId);
        }

        device.get().setDescription(deviceDTO.getDescription());
        device.get().setLocation(deviceDTO.getLocation());
        device.get().setMaximumEnergyConsumption(deviceDTO.getMaximumEnergyConsumption());
        device.get().setAverageEnergyConsumption(deviceDTO.getAverageEnergyConsumption());

        deviceRepository.save(device.get());

        LOGGER.debug("Device with id {} was updated in db", deviceId);
        return deviceId;
    }

    public void delete(UUID deviceId) {
        Optional<Device> device = deviceRepository.findById(deviceId);
        if (!device.isPresent()) {
            LOGGER.error("Device with id {} was not found in db", deviceId);
            throw new ResourceNotFoundException(Device.class.getSimpleName() + " with id: " + deviceId);
        }

        deviceRepository.deleteById(device.get().getId());
    }
}
