package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MonitorValueDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.services.MonitorValueService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/monitorValue")
public class MonitorValueController {
    private final MonitorValueService monitorValueService;

    @Autowired
    public MonitorValueController(MonitorValueService monitorValueService) {
        this.monitorValueService = monitorValueService;
    }

    @GetMapping()
    public ResponseEntity<List<MonitorValueDTO>> getMonitorValues() {
        List<MonitorValueDTO> dtos = monitorValueService.findMonitorValues();
        for (MonitorValueDTO dto : dtos) {
            Link monitorValueLink = linkTo(methodOn(MonitorValueController.class)
                    .getMonitorValue(dto.getId())).withRel("sensorDetails");
            dto.add(monitorValueLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertMonitorValue(@Valid @RequestBody MonitorValueDTO monitorValueDTO) {
        System.out.println(monitorValueDTO.toString());
        UUID sensorID = monitorValueService.insert(monitorValueDTO);
        return new ResponseEntity<>(sensorID, HttpStatus.CREATED);
    }

    @PostMapping(value = "insertCurrentMonitorValueForAccount/{id}")
    public ResponseEntity<List<MonitorValueDTO>> insertCurrentMonitorValueForAccount(@PathVariable("id") UUID accountId, @RequestBody Object o) {
        System.out.println(accountId);
        List<MonitorValueDTO> dtos = monitorValueService.insertCurrentMonitorValueForAccount(accountId);
        for (MonitorValueDTO dto : dtos) {
            Link monitorValueLink = linkTo(methodOn(MonitorValueController.class)
                    .getMonitorValue(dto.getId())).withRel("sensorDetails");
            dto.add(monitorValueLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping(value = "/{sensorsMonitorValue}")
    public ResponseEntity<UUID> insertSensorsMonitorValue(@Valid @RequestBody MonitorValueDTO monitorValueDTO) {
        UUID sensorID = monitorValueService.insertSensorsMonitorValue(monitorValueDTO);
        return new ResponseEntity<>(sensorID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MonitorValueDTO> getMonitorValue(@PathVariable("id") UUID monitorValueId) {
        MonitorValueDTO dto = monitorValueService.findMonitorValueById(monitorValueId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    /*@GetMapping(value = "/getMonitorValueByDate/{date}")
    public ResponseEntity<List<MonitorValueDTO>> getMonitorValueByDate(@PathVariable("date")  String date) {
        System.out.println("\n\n\n\n\n" + date);
        List<MonitorValueDTO> dto = new ArrayList<>();
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }*/

    //TODO: UPDATE, DELETE per resource

    @PutMapping()
    public ResponseEntity<UUID> updateMonitorValue(@Valid @RequestBody MonitorValueDTO monitorValueDTO) {
        UUID monitorValueId = monitorValueService.update(monitorValueDTO);
        return new ResponseEntity<>(monitorValueId, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteMonitorValue(@PathVariable("id") UUID monitorValueId) {
        monitorValueService.delete(monitorValueId);
        return new ResponseEntity(HttpStatus.OK);
    }
}
