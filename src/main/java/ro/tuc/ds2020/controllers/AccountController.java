package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.AccountDetailsDTO;
import ro.tuc.ds2020.dtos.AccountDeviceIdsDTO;
import ro.tuc.ds2020.services.AccountService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/account")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping()
    public ResponseEntity<List<AccountDTO>> getAccounts() {
        List<AccountDTO> dtos = accountService.findAccounts();
        for (AccountDTO dto : dtos) {
            Link accountLink = linkTo(methodOn(AccountController.class)
                    .getAccount(dto.getId())).withRel("accountDetails");
            dto.add(accountLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/allClients")
    public ResponseEntity<List<AccountDetailsDTO>> getClientAccounts() {
        List<AccountDetailsDTO> dtos = accountService.findClientAccounts();
        List<AccountDetailsDTO> dtosCopy = accountService.findClientAccounts();
        System.out.println(dtos);
        for (AccountDetailsDTO dto : dtos) {
            Link accountLink = linkTo(methodOn(AccountController.class)
                    .getAccount(dto.getId())).withRel("accountDetails");
                System.out.println(dto.getType());
                dto.add(accountLink);
                dtosCopy.removeIf(dto1 -> !dto1.getType().equals("CLIENT"));
        }
        return new ResponseEntity<>(dtosCopy, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody AccountDetailsDTO accountDTO) {
        System.out.println(accountDTO.toString());
        UUID accountID = accountService.insert(accountDTO);
        return new ResponseEntity<>(accountID, HttpStatus.CREATED);
    }

    @PostMapping(value = "/addDeviceToClient")
    public ResponseEntity<UUID> addDeviceToClient(@RequestBody AccountDeviceIdsDTO accountDeviceIdsDTO) {
        UUID accountID = accountService.insertDeviceToClient(accountDeviceIdsDTO);
        return new ResponseEntity<>(accountID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AccountDetailsDTO> getAccount(@PathVariable("id") UUID accountId) {
        AccountDetailsDTO dto = accountService.findAccountById(accountId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    //TODO: UPDATE, DELETE per resource

    @PutMapping()
    public ResponseEntity<UUID> updateAccount(@Valid @RequestBody AccountDetailsDTO accountDTO) {
        UUID accountId = accountService.update(accountDTO);
        return new ResponseEntity<>(accountId, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteAccount(@PathVariable("id") UUID accountId) {
        accountService.delete(accountId);
        return new ResponseEntity(HttpStatus.OK);
    }

}
