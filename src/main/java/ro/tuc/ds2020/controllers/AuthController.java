package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.AccountDetailsDTO;
import ro.tuc.ds2020.dtos.AccountLoginDTO;
import ro.tuc.ds2020.dtos.builders.AccountBuilder;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.services.AccountService;

import java.util.UUID;


@RestController
@CrossOrigin
@RequestMapping(value = "/auth")
public class AuthController {
    private final AccountService accountService;

    @Autowired
    public AuthController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<AccountDetailsDTO> login(@RequestBody AccountLoginDTO accountLoginDTO) {
        System.out.println(accountLoginDTO);

        String accountName = accountLoginDTO.getAccountName();
        String password = accountLoginDTO.getPassword();

        if (accountName == null || accountName == "") {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
        if (password == null || password == "") {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        AccountDetailsDTO accountDetailsDTO = accountService.findAccountByUsername(accountName);
        boolean isPasswordMatch = password.matches(accountDetailsDTO.getPassword());

        if (accountDetailsDTO != null && isPasswordMatch) {
            return new ResponseEntity<>(accountDetailsDTO, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}
