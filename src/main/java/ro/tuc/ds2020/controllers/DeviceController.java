package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceSensorIdsDTO;
import ro.tuc.ds2020.services.DeviceService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/device")
public class DeviceController {
    
    private final DeviceService deviceService;

    @Autowired
    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @GetMapping()
    public ResponseEntity<List<DeviceDTO>> getDevices() {
        List<DeviceDTO> dtos = deviceService.findDevices();
        for (DeviceDTO dto : dtos) {
            Link deviceLink = linkTo(methodOn(DeviceController.class)
                    .getDevice(dto.getId())).withRel("deviceDetails");
            dto.add(deviceLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/getAvailableDevices")
    public ResponseEntity<List<DeviceDTO>> getAvailableDevices() {
        List<DeviceDTO> dtos = deviceService.findAvailableDevices();
        for (DeviceDTO dto : dtos) {
            Link deviceLink = linkTo(methodOn(DeviceController.class)
                    .getDevice(dto.getId())).withRel("deviceDetails");
            dto.add(deviceLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/getAvailableDevicesWithAccount")
    public ResponseEntity<List<DeviceDTO>> getAvailableDevicesWithAccount() {
        List<DeviceDTO> dtos = deviceService.getAvailableDevicesWithAccount();
        for (DeviceDTO dto : dtos) {
            Link deviceLink = linkTo(methodOn(DeviceController.class)
                    .getDevice(dto.getId())).withRel("deviceDetails");
            dto.add(deviceLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertDevice(@Valid @RequestBody DeviceDTO deviceDTO) {
        UUID deviceID = deviceService.insert(deviceDTO);
        return new ResponseEntity<>(deviceID, HttpStatus.CREATED);
    }

    @PostMapping(value = "/addSensorToDevice")
    public ResponseEntity<UUID> addDSensorToDevice(@RequestBody DeviceSensorIdsDTO deviceSensorIdsDTO) {
        System.out.println(deviceSensorIdsDTO.sensorId);
        UUID accountID = deviceService.insertSensorToDevice(deviceSensorIdsDTO);
        return new ResponseEntity<>(accountID, HttpStatus.CREATED);
    }

    @PostMapping(value = "/{clientDevice}")
    public ResponseEntity<UUID> insertClientDevice(@Valid @RequestBody DeviceDTO deviceDTO) {
        UUID deviceID = deviceService.insertClientDevice(deviceDTO);
        return new ResponseEntity<>(deviceID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DeviceDTO> getDevice(@PathVariable("id") UUID deviceId) {
        DeviceDTO dto = deviceService.findDeviceById(deviceId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    /*@GetMapping(value = "/getSensorByDevice/{id}")
    public ResponseEntity<SensorDTO> getSensorByDevice(@PathVariable("id") UUID deviceId) {
        SensorDTO dto = deviceService.findSensorByDeviceId(deviceId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }*/

    //TODO: UPDATE, DELETE per resource

    @PutMapping()
    public ResponseEntity<UUID> updateDevice(@Valid @RequestBody DeviceDTO deviceDTO) {
        UUID deviceId = deviceService.update(deviceDTO);
        return new ResponseEntity<>(deviceId, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteDevice(@PathVariable("id") UUID deviceId) {
        deviceService.delete(deviceId);
        return new ResponseEntity(HttpStatus.OK);
    }
}
