package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.StreamingHttpOutputMessage;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.SensorDeviceDTO;
import ro.tuc.ds2020.services.DeviceService;
import ro.tuc.ds2020.services.SensorService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/sensor")
public class SensorController {

    private final SensorService sensorService;

    @Autowired
    public SensorController(SensorService sensorService) {
        this.sensorService = sensorService;
    }

    @GetMapping()
    public ResponseEntity<List<SensorDTO>> getSensors() {
        List<SensorDTO> dtos = sensorService.findSensors();
        for (SensorDTO dto : dtos) {
            Link sensorLink = linkTo(methodOn(SensorController.class)
                    .getSensor(dto.getId())).withRel("sensorDetails");
            dto.add(sensorLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/getAvailableSensors")
    public ResponseEntity<List<SensorDTO>> getAvailableSensors() {
        List<SensorDTO> dtos = sensorService.findAvailableSensors();
        for (SensorDTO dto : dtos) {
            Link sensorLink = linkTo(methodOn(SensorController.class)
                    .getSensor(dto.getId())).withRel("sensorDetails");
            dto.add(sensorLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertSensor(@Valid @RequestBody SensorDTO sensorDTO) {
        System.out.println(sensorDTO.toString());
        UUID sensorID = sensorService.insert(sensorDTO);
        return new ResponseEntity<>(sensorID, HttpStatus.CREATED);
    }

    @PostMapping(value = "/{devicesSensor}")
    public ResponseEntity<UUID> insertDevicesSensor(@RequestBody SensorDeviceDTO sensorDevice) {
        // System.out.println("\n\n\n\n\n\n" + sensorDevice);
        UUID sensorID = sensorService.insertDevicesSensor(sensorDevice.sensor, sensorDevice.deviceId);
        //return new ResponseEntity<>(sensorID, HttpStatus.CREATED);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<SensorDTO> getSensor(@PathVariable("id") UUID sensorId) {
        SensorDTO dto = sensorService.findSensorById(sensorId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    //TODO: UPDATE, DELETE per resource

    @PutMapping()
    public ResponseEntity<UUID> updateSensor(@Valid @RequestBody SensorDTO sensorDTO) {
        UUID sensorId = sensorService.update(sensorDTO);
        return new ResponseEntity<>(sensorId, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteSensor(@PathVariable("id") UUID sensorId) {
        sensorService.delete(sensorId);
        return new ResponseEntity(HttpStatus.OK);
    }

}
