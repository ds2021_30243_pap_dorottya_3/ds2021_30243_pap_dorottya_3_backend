package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.AccountDetailsDTO;
import ro.tuc.ds2020.services.AccountService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class AccountControllerUnitTest extends Ds2020TestConfig {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService service;

    @Test
    public void insertAccountTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        AccountDetailsDTO accountDTO = new AccountDetailsDTO("John", "Somewhere Else street", 22, "account1", "password1", "client", null);

        mockMvc.perform(post("/account")
                .content(objectMapper.writeValueAsString(accountDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

    @Test
    public void insertAccountTestFailsDueToAge() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        AccountDetailsDTO accountDTO = new AccountDetailsDTO("John", "Somewhere Else street", 17, "account1", "password2", "client", null);

        mockMvc.perform(post("/account")
                .content(objectMapper.writeValueAsString(accountDTO))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void insertAccountTestFailsDueToNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        AccountDetailsDTO accountDTO = new AccountDetailsDTO("John", null, 17, "account1", "password2", "client", null);

        mockMvc.perform(post("/account")
                .content(objectMapper.writeValueAsString(accountDTO))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }
}
