package ro.tuc.ds2020.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.AccountDetailsDTO;

import static org.springframework.test.util.AssertionErrors.assertEquals;

import java.util.HashSet;
import java.util.List;
import java.util.UUID;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/delete.sql")
public class AccountServiceIntegrationTests extends Ds2020TestConfig {

    @Autowired
    AccountService accountService;

    @Test
    public void testGetCorrect() {
        List<AccountDTO> accountDTOList = accountService.findAccounts();
        assertEquals("Test Insert Account", 1, accountDTOList.size());
    }

    @Test
    public void testInsertCorrectWithGetById() {
        AccountDetailsDTO p = new AccountDetailsDTO("John", "Somewhere Else street", 22, "account1", "password1", "client", new HashSet<>());
        UUID insertedID = accountService.insert(p);

        AccountDetailsDTO insertedAccount = new AccountDetailsDTO(insertedID, p.getName(), p.getAddress(), p.getAge(), p.getAccountName(), p.getPassword(), p.getType(), p.getDevices());
        AccountDetailsDTO fetchedAccount = accountService.findAccountById(insertedID);

        assertEquals("Test Inserted Account", insertedAccount, fetchedAccount);
    }

    @Test
    public void testInsertCorrectWithGetAll() {
        AccountDetailsDTO p = new AccountDetailsDTO("John", "Somewhere Else street", 22, "account1", "password1", "client", new HashSet<>());
        accountService.insert(p);

        List<AccountDTO> accountDTOList = accountService.findAccounts();
        assertEquals("Test Inserted Accounts", 5, accountDTOList.size());
    }
}
